
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
      //title: process.env.npm_package_name || 'Wydaj-Wyrok.pl',
    title: 'Teraz Ty bądź Sędzią! Symulator rozpraw sądowych online.',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: 'Jak zapadają wyroki w sprawach karnych? Co ma wpływ na wymiar kary? Jakie okoliczności decydują o wymiarze kary? Teraz Ty bądź Sędzią! to wideo symulator rozpraw sądowych, innowacyjne narzędzie edukacyjne w którym użytkownik wciela się w rolę sędziego.' },
      { name: 'keywords', content: 'sąd, sędzia, wyrok, kodeks karny, kara, pozbawienie wolności, edukacja, prawo, Fundacja ProPublika, wymiar sprawiedliwości' },
      { name: 'copyright', content: 'Fundacja ProPublika' },
      { hid: 'og:title', property: 'og:title', content: "Właśnie wydałem wyrok!" },
      { hid: 'og:description', property: 'og:description', content: "Teraz Ty wciel się w rolę sędziego! Jaka będzie Twoja decyzja?" },
      { hid: 'og:url', property: 'og:url', content: "https://www.wydaj-wyrok.pl" },
      { hid: 'og:image', property: 'og:image', content: "https://www.wydaj-wyrok.pl/shareFB.png" },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-TileImage', content: '/ms-icon-144x144.png' },
      { name: 'theme-color', content: '#ffffff' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '57x57', href: '/apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '60x60', href: '/apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '72x72', href: '/apple-icon-72x72.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '76x76', href: '/apple-icon-76x76.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '114x114', href: '/apple-icon-114x114.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '120x120', href: '/apple-icon-120x120.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '144x144', href: '/apple-icon-144x144.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '152x152', href: '/apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', type: 'image/x-icon', sizes: '180x180', href: '/apple-icon-180x180.png' },
      { rel: 'icon', type: 'image/x-icon', sizes: '192x192', href: '/android-icon-192x192.png' },
      { rel: 'icon', type: 'image/x-icon', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/x-icon', sizes: '96x96', href: '/favicon-96x96.png' },
      { rel: 'icon', type: 'image/x-icon', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" }
    ]
  },


/*
 *
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 *
 *
 */



  server: {
    port: 3009, // default: 3000
    host: 'localhost', // default: localhost
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@assets/css/global.css',
    '@assets/fonts/icons.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~plugins/vue-yt-embed.js", ssr: false },
    { src: "~plugins/vue-google-charts.js", ssr: false },
    { src: "~plugins/vue-swiper.js", ssr: false },
    { src: "~plugins/vue-window-size.js", ssr: false },
    { src: "~plugins/vue-match-heights.js", ssr: false },
    { src: "~plugins/vue-img.js", ssr: false },
    { src: "~plugins/mobile-orientation.js", ssr: false },
    { src: '~plugins/ga.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
  ],
  env: {
    baseUrl: '',
    apiUrl: 'https://apiv02.wydaj-wyrok.pl',
    imagesUrl: '',

  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
