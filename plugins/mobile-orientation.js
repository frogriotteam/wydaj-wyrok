
// Listen for orientation changes
window.addEventListener("orientationchange", function() {
    // Announce the new orientation number
    
  }, false);
  
  // Listen for resize changes
  window.addEventListener("resize", function() {
    // Get screen size (inner/outerWidth, inner/outerHeight)
    
  }, false);
  
  // Find matches
  var mql = window.matchMedia("(orientation: portrait)");
  
  // If there are matches, we're in portrait
  if(mql.matches) {  
    // Portrait orientation
    window.document.body.classList.add('portrait')
    window.document.body.classList.remove('landscape')

  } else {  
    // Landscape orientation
    window.document.body.classList.add('landscape')
    window.document.body.classList.remove('portrait')

  }
  
  // Add a media query change listener
  mql.addListener(function(m) {
    if(m.matches) {
        window.document.body.classList.add('portrait')
        window.document.body.classList.remove('landscape')

    }
    else {
        window.document.body.classList.add('landscape')
        window.document.body.classList.remove('portrait')

    }
  });
