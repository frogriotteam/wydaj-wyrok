import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    state: {
      slideIndex: Math.floor(Math.random() * 9),
      lawCaseId: 2,
      vote: {}
    },
    mutations: {
      setSlideIndex (state, index) {
        state.slideIndex = index
      },
      setLawCaseId (state, index) {
        state.lawCaseId = index
      },
      setVote (state, index) {
        state.vote = index
      }
    },
    actions: {
      setSlideIndex(vuexContext, data) {
        vuexContext.commit('setSlideIndex', data);
      },
      setLawCaseId(vuexContext, data) {
      vuexContext.commit('setLawCaseId', data);
      },
      setVote(vuexContext, data) {
        vuexContext.commit('setVote', data);
      }
    },
    getters: {
      slideIndex: state =>  {
        return state.slideIndex
      },
      lawCaseId: state => {
        return state.lawCaseId
      },
      vote: state => {
        return state.vote
      }
    }
  })
}

export default createStore;